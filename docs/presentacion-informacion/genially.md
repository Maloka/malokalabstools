---
tags:
    - Presentación
    - Información
    - Dinámica
    - Interactividad
---
# Genially

![imagen de Genially](genially.jpg)

Es una herramienta que permite diseñar y generar contenidos digitales interactivo, presentando información de manera dinámica, sin necesidad de programar y sin tener conocimientos avanzados de diseño. 

[Aquí encontrarás el enlace oficial de Genially, dale clic para ir](https://genial.ly/es/)

## Tutorial 

Ahora, para que seas todo un experto en la presentación de la información de manera dinámica y divertida, te presentamos un pequeño tutorial de cómo empezar a usar Genially en diferentes escenarios.
<iframe width="560" height="315" src="https://www.youtube.com/embed/6gfp4zxjtf0" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

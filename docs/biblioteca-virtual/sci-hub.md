---
tags:
    - Información académica
    - Información científica
    - Artículo
    - Biblioteca
---

# Sci-Hub

Es un repositorio o biblioteca virtual pirata que alberga más de 84 millones de artículos académicos.
![](scihub.jpg)

[Aquí encontrarás uno de los tantos enlaces para acceder a Sci-Hub, dale clic para ir](https://sci-hub.se/)

## Tutorial 

Ahora, para que seas todo un experto en la busqueda de información, te compartimos un pequeño tutorial

<iframe width="560" height="315" src="https://www.youtube.com/embed/1ylzZ6lkFbA" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

---
tags:
    - Emprendimientos
    - Causas sociales
    - Iniciativas
    - Crowfonding
---

# Kickstarter

Kickstarter es una corporación de beneficio público, que mantiene una plataforma global de micromecenazgo para proyectos creativos. ​​La misión declarada de la empresa es "ayudar a dar vida a proyectos creativos"
![](kickstarter.jpg)

[Aquí encontrarás el enlace oficial de Kickstarter, dale clic para ir](https://www.kickstarter.com)

## Tutorial 

Ahora, te presentamos un pequeño tutorial de cómo empezar a usar Kickstarter en diferentes escenarios.
<iframe width="560" height="315" src="https://www.youtube.com/embed/vah0IudrKPY" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

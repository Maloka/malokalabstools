---
tags:
    - Idiomas
    - Juegos
    - Aprendizaje
    
---

# Duolingo

![](duolingo.jpg)

Es una herramienta educativa que ayuda a aprender un nuevo idioma mediante pequeñas y cortas lecciones interactivas que se componen de escritura, escucha y lectura de cada concepto. Es posible estudiar y aprender más de 100 curos de 28 idiomas disponibles. 

[Aquí encontrarás el enlace oficial de Duolingo para que pongas a prueba tus conocimientos y te diviertas, dale clic para ir](www.duolingo.com/) 


# Tutorial 

Ahora, para que seas todo un experto en muchos idiomas, te presentamos un pequeño tutorial de cómo usar Duolingo. 
<iframe width="560" height="315" src="https://www.youtube.com/embed/STjVRnf5Xp8" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

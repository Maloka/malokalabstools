---
tags:
    - Idiomas
    - Aprendizaje
    - Inglés
    - Gramática
---

# Grammarly

![](grammarly.png)

Es una herramienta online e instalable de revisión gramatical que sirve para corregir ortografía, redacción y estilo en inglés a medida que se van escribiendo textos sobre el editor. De igual forma, permite copiar o adjuntar textos para hacer la revisión gramatical. 

La herramienta también posee la opción de agregar extensiones a paquetes de Office, Google, entre otros para permitir hacer una verificación inmediata de los textos que ahí se editen. 

[Aquí encontrarás el enlace oficial de Grammarly, dale clic para ir](https://www.grammarly.com/)


## Tutorial 

Ahora, para que seas todo un experto en la revisión ortográfica y gramatical de tus textos en inglés, te presentamos un pequeño tutorial de cómo empezar a usar Grammarly en diferentes escenarios.
<iframe width="560" height="315" src="https://www.youtube.com/embed/syLGlHLAJls" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

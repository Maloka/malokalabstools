---
tags:
  - programación
  - música
  - producción de sonidos
---

# Sonic-Pi

![](sonic.png)

Sonic Pi es un entorno de live coding o codificación en vivo  basado en Ruby, originalmente diseñado para apoyar las clases de informática y música en las escuelas, desarrollado por Sam Aaron en el Laboratorio de Informática de la Universidad de Cambridge en colaboración con la Fundación Raspberry Pi.

[Aquí encontrarás el enlace oficial de Sonic-Pi, dale clic para ir](https://sonic-pi.net/)


## Tutorial 

Ahora, para que seas todo un experto en la creación de música, te presentamos un pequeño tutorial de cómo empezar a usar Sonic-Pi en diferentes escenarios. <iframe width="560" height="315" src="https://www.youtube.com/embed/cydH_JAgSfg" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

---
tags:
  - Programación
  - Juegos
  - Lenguajes de programación
---


# Desarrolla lógica y aprender a programar jugando 

Blockly Games es un proyecto de Google que presenta diferentes juegos para desarrollar la lógica y aprender diversos lenguaje de programación de manera divertida.

![blockly](blocky.png)

[Aquí encontrarás el enlace oficial para acceder, dale clic para ir](https://blockly.games/)

## Tutorial 

Ahora, para que seas todo una experta jugando y aplicando diferentes lenguajes de programación, te presentamos un pequeño tutorial de cómo empezar a usar esta herramienta.

<iframe width="560" height="315" src="https://www.youtube.com/embed/rCFFyfFtpg0" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

---
tags:
  - Programación
  - Biblioteca JavaScript
  - Programación artística
---


# P5..js

¡p5.js es una biblioteca de JavaScript para la programación creativa, que busca hacer que programar sea accesible e inclusivo para artistas, diseñadores, educadores, principiantes y cualquier otra persona! p5.js es gratuito y de código abierto porque creemos que el software y las herramientas para aprenderlo deben ser accesibles para todos.

![](p5.png)

[Aquí encontrarás el enlace oficial para acceder, dale clic para ir](https://p5js.org/es/)

## Tutorial 

Ahora, para que seas todo un experto en la programación creativa te presentamos un pequeño tutorial de cómo empezar a usar esta herramienta en diferentes escenarios.

<iframe width="560" height="315" src="https://www.youtube.com/embed/DtAHvMjMzMQ" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

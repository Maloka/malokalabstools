---
tags:
    - Programación
    - Música
    - Producción
    
---

# ChucK

![](chuck.jpg)

ChucK es un nuevo lenguaje de programación orientado a sonido, que permite generar, grabar, sintetizar sonido en tiempo real. Interactivo, es posible añadir y modificar, el código de programa, mientras ChucK aún está en funcionamiento. Entre sus características más relevantes se puede encontrar, una sintaxis peculiar, en la que to chuck ('arrojar/conectar') se hace especialmente notable. Además puesto que su mayor funcionalidad es el tiempo real, la sincronización y el control del sonido digital al nivel de las muestras, están explícitamente expresadas en el lenguaje.

[Aquí encontrarás el enlace oficial de ChucK, dale clic para ir](https://chuck.stanford.edu/release/) 


# Tutorial 

Ahora, para que seas todo un experto en música, te presentamos un pequeño tutorial de cómo usar ChucK. <iframe width="560" height="315" src="https://www.youtube.com/embed/hFAh-pgxGyc" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>


---
tags:
    - Astronomía
    - Aprendizaje
    - Física
    - Planetas
    - Constelaciones
    - Cielo
    - Cuerpos celestes
---
# Grammarly

![](stellarium.jpg)

Es una app que funciona como un mapa de estrellas o mapa estelar interactivo, donde se identifican constelaciones, estrellas, planetas e incluso satélites. También permite introducir coordenadas exactas para ver cómo se vería el cielo desde un lugar específico.   

[Aquí encontrarás el enlace oficial para que puedas descargar Stellarium en tus dispositivos, dale clic para ir](https://play.google.com/store/apps/details?id=com.noctuasoftware.stellarium_free)

## Tutorial 

Ahora, para que seas todo un experto ubicando planetas y satélites, así como identificando estrellas, te presentamos un pequeño tutorial de cómo empezar a usar Stellarium. 
<iframe width="560" height="315" src="https://www.youtube.com/embed/6Aam6JzeZ8k" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

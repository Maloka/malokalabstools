---
tags:
    - Gestión de proyectos
    - Organización 
    - Gestión de actividades
    
---

# Workflowy

![](WorkFlowy.png)

Es un sistema de gestión que posibilita automatizar procesos o un conjunto de tareas de forma sencilla e interactiva

[Aquí encontrarás el enlace oficial de Worflowy, dale clic para ir](https://workflowy.com/)


## Tutorial 

Ahora, para que seas todo un experto en la gestión de tus actividades y proyectos, te presentamos un pequeño tutorial de cómo usar Workflowy en diferentes escenarios. 
<iframe width="560" height="315" src="https://www.youtube.com/embed/iC5i_RVOYI0" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

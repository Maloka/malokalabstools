---
tags:
    - Gestión de proyectos
    - Organización 
    - Gestión de actividades
    
---

# Trello

![](trello.png)

Es una app que permite gestionar proyectos y actividades de manera sencilla, que permite organizar actividades por realizar, tareas profesionales y personales. Esta diseñada como una especide de Post It donde se organizan de acuerdo a prioriodades y fechas, etc. 

[Aquí encontrarás el enlace oficial de Trello, dale clic para ir](https://trello.com/es)


## Tutorial 

Ahora, para que seas todo un experto en la gestión de tus actividades y proyectos, te presentamos un pequeño tutorial de cómo usar Trello en diferentes escenarios. <iframe width="560" height="315" src="https://www.youtube.com/embed/X-jUv_Shc9M" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

---
tags:
    - Gamificación
    - Juegos
    - Aprendizaje
    
---

# Kahoot

![](kahoot.png)

Es una herramienta para diseñar juegos que a modo de quices les permita a estudiantes y docentes qué tanto han apropiado un concepto o temática. En las preguntas se pueden incluir imágenes, videos y sonidos y los resultados se muestran en línea y pueden ser descargados en una hoja de excel para llevar un control por parte del moderador. 

[Aquí encontrarás el enlace oficial de Kahoot para que pongas a prueba tus conocimientos y te diviertas, dale clic para ir](https://kahoot.it/) y [kahoot](https://kahoot.com/)



# Tutorial 

Ahora, para que seas todo un experto en la gamificación, te presentamos un pequeño tutorial de cómo empezar a usar Kahoot en diferentes escenarios. <iframe width="560" height="315" src="https://www.youtube.com/embed/pANtMqNWBek" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

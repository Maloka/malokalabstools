# Herramientas Tecnológicas

En este sitio encontrarás varias herramientas que te pueden servir para comunicar ideas, gestionar tiempo, leer, aprender a programar y mucho más,
si están interesada o interesado en nutrir esta caja de herramientas con aquellas que usas y te gustaría compartir, te invitamos a llenar el siguiente buzón:

## Buzón para sugerir aplicaciones

<a href="https://forms.gle/5y7TLhWXZpuFK1n3A">
<img src="./buzon-sugerencias.gif" alt="./buzon-sugerencias.gif" style="width:40%;">
</a>

<!-- <iframe src="https://docs.google.com/forms/d/e/1FAIpQLScNWaIzGC7TGp3Hv6im1OMka6yXnUllngtZmYpsRO0Q0BVsVg/viewform?embedded=true" width="640" height="2621" frameborder="0" marginheight="0" marginwidth="0">Cargando…</iframe> -->

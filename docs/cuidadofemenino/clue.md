---
tags:
    - Autocuidado
    - Autoconocimiento
    - Ciclo menstrual
    - Monitoreo ciclo menstrual
    - Conocimiento femenino
    - Empoderamiento 
---

# Clue

![](clue.jpg)

Clue es una aplicación de salud menstrual, sirve para monitorear el ciclo, llevar registros y es usada como herramienta para tomar decisiones basadas en los datos almacenados en la app. 

[Aquí encontrarás el enlace oficial de Clue] (https://helloclue.com/es) 

[Y aquí encontrarás el enlace para que puedas descargarla en tus dispositivos](https://play.google.com/store/apps/details?id=com.clue.android&hl=es&gl=US)



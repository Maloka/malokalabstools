---
tags:
    - Videos
    - Edición
    - presentaciones
---

# Powtoon

Es una herramienta de presentación en línea que permite crear presentaciones animadas o videos con diferentes tipos de ediciones. Posee diferentes plantillas para estudiantes, profesores, administrativos, artistas, etc. Aquí pueden crear sus propios personajes, imágenes, animaciones, texto, videos, música y cargar su propia voz en off.

![](powtoon.png)

[Aquí encontrarás el enlace oficial de Powtoon, dale clic para ir](https://www.powtoon.com/)

## Tutorial 

Ahora, para que seas todo un experto en el diseño y edición de videos, te presentamos un pequeño tutorial de cómo empezar a usar Powtoon en diferentes escenarios.
<iframe width="560" height="315" src="https://www.youtube.com/embed/G_02pj2q0D8" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

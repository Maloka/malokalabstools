---
tags:
    - Videos
    - Edición
    - presentaciones
---

# Videoscribe

Es un programa para crear videos animados de forma rápida y fácil, con la particularidad de que los elementos del video pareciera que fueran dibujados a mano a medida que van apareciendo de forma progresiva, permitiendo mezclar textos y conceptos complejos con audios y imágenes.

[Enlace oficial de la plataforma dale clic para ir](https://www.videoscribe.co/en/free-trial/)

![imagen de videoscribe](videoscribe.jpeg)

## Aquí un pequeño tutorial

<iframe width="560" height="315" src="https://www.youtube.com/embed/dXCVFpdBRmI" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>




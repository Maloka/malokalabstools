---
tags:
    - Diseño 3D
    - Dibujo
    - Modelado
    - Impresión 3D
---

# Tinkercad

![](tinkercad.jpg)

Es un software de diseño y modelado 3D de fácil uso que permite diseñar objetos para luego imprimirlos a través de una impresora 3D. Se puede crear desde juguetes, prototipos, modelos de robots, entre otros.

[Aquí encontrarás el enlace oficial de Tinkercad, dale clic para ir](https://www.tinkercad.com/)


## Tutorial 

Ahora, para que seas todo un experto en el diseño, modelado y creación de tus poryectos 3D te presentamos un pequeño tutorial de cómo empezar a usar Tinkercad en diferentes escenarios.
<iframe width="560" height="315" src="https://www.youtube.com/embed/_TML2dqyaNg" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

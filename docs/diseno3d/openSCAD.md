---
tags:
    - Diseño 3D
    - Dibujo
    - Modelado
    - Impresión 3D
---

# OpenSCAD

![](Openscad.png)

OpenSCAD es una aplicación libre para crear objetos sólidos de CAD. No es un editor interactivo sino un compilador 3D basado en un lenguaje de descripción textual. Un documento de OpenSCAD especifica primitivas geométricas y define como son modificadas y manipuladas para reproducir un modelo 3D.

[Aquí encontrarás el enlace oficial de Tinkercad, dale clic para ir](https://openscad.org/)


## Tutorial 

Ahora, para que seas todo un experto en el diseño, modelado y creación de tus pyoyectos 3D te presentamos un pequeño tutorial de cómo empezar a usar  Openscad en diferentes escenarios.

<iframe width="560" height="315" src="https://www.youtube.com/embed/uYVDYicKUbM" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
